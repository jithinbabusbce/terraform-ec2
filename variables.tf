variable "AWS_REGION" {
  description = "Region everything is executed in."
  default     = "us-east-2"
}

variable "vpc-cidr" {
  description = "VPC CIDR Block"
}

variable "env" {
  description = "ENV Name"
}

variable "Private-type" {
  description = "Private EC2 instance type"
}

variable "Private-ami" {
  description = "Public key Name"
}

variable "vpn-type" {
  description = "VPN Server instance type"
}

variable "vpn-ami" {
  description = "VPN Server AMI"
}

variable "NAME_OF_PRIVATE_KEY" {
  description = "Private key Name"
}

variable "NAME_OF_PUBLIC_KEY" {
  description = "Public key Name"
}

variable "instance_count" {
  description = "EC2 instance count"
}






