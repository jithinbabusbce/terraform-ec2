## Private Server
resource "aws_instance" "Privatecount-server" {
  count         = "${var.instance_count}"
  ami           = "${var.Private-ami}"
  instance_type = "${var.Private-type}"

  tags = {
    Name = "${var.env}-Private-server-${count.index}"
    env  = "${var.env}"
  }

  ## Root Volume
  root_block_device {
    volume_size           = 30
    volume_type           = "gp2"
    delete_on_termination = true
  }

  # the VPC subnet
  subnet_id = "${aws_subnet.Subnet-private-1.id}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.private-server.id}"]

  # the key
  key_name = "${var.NAME_OF_PRIVATE_KEY}"

}
