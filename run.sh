#!/bin/bash
terraform init
terraform get -update
terraform apply -input=false -var-file=terraform.tfvars  -auto-approve
bash ansible.sh
bash vpnsetup.sh