output "private_ips" {
  value = "${aws_instance.Private-server.private_ip}"
}

output "count-private_ips" {
  value = "${aws_instance.Privatecount-server.*.private_ip}"
}


output "public_ips" {
  value = "${aws_eip.vpn-server-eip.public_ip}"
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  value       = "${aws_vpc.VPC.cidr_block}"
}


