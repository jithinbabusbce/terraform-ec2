# Steps to follow for template deployment

Note: Follow creation and deletion steps mentioned in the readme file

### Prerequisites
1. terraform
2. AWS authentication in terminal
   ``` aws configure ```


### For Running and creating the resources
``` bash run.sh```

#### For connecting to VPN
* Access the web UI of VPN
```sh
   https://<IP>:943
   Example: https://3.15.178.8:943
   Give username and password: openvpn/openvpn
   Download the ovpn client 
```

* Connect openvpn from terminal
```sh
   For Ubuntu: sudo openvpn --config client.ovpn
   For Mac and Windows - Use Openvpn Client, Download from https://<IP>:943
```

## Ansible host file
  Location: ansible/hosts

## Parameter file name
   ``` terraform.tfvars  ```


## For SSH into the server
``` ssh -i ec2key ubuntu@<IP> ```

## For destroying all resources created
``` bash destroy.sh ```





