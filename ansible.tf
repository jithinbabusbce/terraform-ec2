## Ansible script

resource "null_resource" "ansible-scripts" {
  depends_on = ["aws_instance.Private-server"]


  provisioner "local-exec" {
    command = "bash ansible.sh"
  }
}
