#!/bin/bash

terraform destroy -var-file=terraform.tfvars -auto-approve
echo "[private]" > ansible/hosts