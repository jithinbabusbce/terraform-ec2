############################ EC2 Server Security Group ########################
resource "aws_security_group" "private-server" {
  depends_on  = ["aws_vpc.VPC"]
  vpc_id      = "${aws_vpc.VPC.id}"
  name        = "${var.env}-private-server-SG"
  description = "Security group that allows needed ports and all egress traffic for EC2 Server"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "ssh port"
    cidr_blocks = ["${var.vpc-cidr}"]
  }

  tags = {
    Name = "${var.env}-EC2-server"
    env  = "${var.env}"
  }
}


######################### Security Group for VPN-Server ####################################
resource "aws_security_group" "vpn-server" {
  vpc_id      = "${aws_vpc.VPC.id}"
  name        = "${var.env}-Openvpn-server-SG"
  description = "Security group that allows needed ports and all egress traffic"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 943
    to_port     = 943
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 1194
    to_port     = 1194
    protocol    = "UDP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.env}-VPN-server"
    env  = "${var.env}"
  }
}
