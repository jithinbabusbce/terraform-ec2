# Internet VPC
resource "aws_vpc" "VPC" {
  cidr_block                       = "${var.vpc-cidr}"
  assign_generated_ipv6_cidr_block = true
  instance_tenancy                 = "default"
  enable_dns_support               = "true"
  enable_dns_hostnames             = "true"
  enable_classiclink               = "false"

  tags = {
    Name = "${var.env}-VPC"
    env  = "${var.env}"
  }
}

# Subnets
resource "aws_subnet" "Subnet-public-1" {
  vpc_id                  = "${aws_vpc.VPC.id}"
  cidr_block              = "${cidrsubnet("${var.vpc-cidr}", 4, 1)}"
  ipv6_cidr_block         = "${cidrsubnet(aws_vpc.VPC.ipv6_cidr_block, 8, 0)}"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-east-2c"

  tags = {
    Name = "${var.env}-VPC-public-1"
    env  = "${var.env}"
  }
}



resource "aws_subnet" "Subnet-private-1" {
  vpc_id                  = "${aws_vpc.VPC.id}"
  cidr_block              = "${cidrsubnet("${var.vpc-cidr}", 4, 4)}"
  ipv6_cidr_block         = "${cidrsubnet(aws_vpc.VPC.ipv6_cidr_block, 8, 3)}"
  map_public_ip_on_launch = "false"
  availability_zone       = "us-east-2c"

  tags = {
    Name = "${var.env}-VPC-private-1"
    env  = "${var.env}"
  }
}


# Internet GW
resource "aws_internet_gateway" "VPC-gw" {
  vpc_id = "${aws_vpc.VPC.id}"

  tags = {
    Name = "${var.env}-VPC"
    env  = "${var.env}"
  }
}

# route tables
resource "aws_route_table" "VPC-public" {
  vpc_id = "${aws_vpc.VPC.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.VPC-gw.id}"
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = "${aws_internet_gateway.VPC-gw.id}"
  }

  tags = {
    Name = "${var.env}-VPC-public-1"
    env  = "${var.env}"
  }
}

# route associations public
resource "aws_route_table_association" "VPC-public-1-a" {
  subnet_id      = "${aws_subnet.Subnet-public-1.id}"
  route_table_id = "${aws_route_table.VPC-public.id}"
}
