#!/bin/bash
vpcid=`terraform output vpc_cidr_block`
ip=`terraform output public_ips`

echo $ip
echo  $vpcid

ssh -o "StrictHostKeyChecking no" -tt -i ec2key openvpnas@$ip <<EOF
cd /usr/local/openvpn_as/scripts
sudo ./sacli --key host.name --value $ip ConfigPut
sudo ./sacli --key "vpn.server.routing.private_network.0" --value $vpcid ConfigPut
sudo ./sacli stop
sudo ./sacli start
exit 0
EOF