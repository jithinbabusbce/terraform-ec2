# nat gateway eip
resource "aws_eip" "nat-IP" {
  vpc = true

  tags = {
    Name = "${var.env}-nat-eip"
    env  = "${var.env}"
  }
}

## Creating nat gateway and attaching eip
resource "aws_nat_gateway" "nat-gw" {
  tags = {
    env  = "${var.env}"
    Name = "${var.env}-nat-gw"
  }

  allocation_id = "${aws_eip.nat-IP.id}"
  subnet_id     = "${aws_subnet.Subnet-public-1.id}"
  depends_on    = ["aws_internet_gateway.VPC-gw"]
}

# Creating route rule using nat
resource "aws_route_table" "VPC-private" {
  vpc_id = "${aws_vpc.VPC.id}"

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.nat-gw.id}"
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = "${aws_internet_gateway.VPC-gw.id}"
  }

  tags = {
    Name = "${var.env}-VPC-private-1"
    env  = "${var.env}"
  }
}

### route associations private
resource "aws_route_table_association" "VPC-private-1-a" {
  subnet_id      = "${aws_subnet.Subnet-private-1.id}"
  route_table_id = "${aws_route_table.VPC-private.id}"
}
