## Private Server
resource "aws_instance" "vpn-server" {
  ami           = "${var.vpn-ami}"
  instance_type = "${var.vpn-type}"

  tags = {
    Name = "${var.env}-vpn-server"
    env  = "${var.env}"
  }

  ## Root Volume
  root_block_device {
    volume_size           = 30
    volume_type           = "gp2"
    delete_on_termination = true
  }

  # the VPC subnet
  subnet_id = "${aws_subnet.Subnet-public-1.id}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.vpn-server.id}"]

  # the key
  key_name = "${var.NAME_OF_PRIVATE_KEY}"

}

resource "aws_eip" "vpn-server-eip" {
  vpc = true
}

resource "aws_eip_association" "vpn-server-eip_assoc" {
  instance_id   = aws_instance.vpn-server.id
  allocation_id = aws_eip.vpn-server-eip.id
}
